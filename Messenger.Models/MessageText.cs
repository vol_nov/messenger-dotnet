﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Messenger.Models
{
    [Serializable]
    public class MessageText : Message
    {
        private string _content;

        public override object Content
        {
            get
            {
                return _content;
            }

            set
            {
                _content = value.ToString();
            }
        }

        public MessageText()
        {

        }
    }
}
