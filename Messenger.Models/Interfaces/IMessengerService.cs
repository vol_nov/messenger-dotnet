﻿using Messenger.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Messenger.Models
{
    /// <summary>
    // Див. ClientMessengerService
    /// </summary>
    public interface IMessengerService
    {
        SignInResult SignIn(string username, string password);
        bool IsUniqueUsername(string username);
        RegisterResult Register(string username, string password);
        string[] GetContacts(string username);
        string[] FindContacts(string username);
        void AddContact(string username, string contact);
        void RemoveContact(string sender, string receiver);
        int SendMessage(Message message);
        Message[] GetNewMessages(string sender, string receiver, int lastMsgId);
        void GoOffline(string username);
    }
}
