﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Messenger.Models.Enums
{
    //Результат реєстрації
    public enum RegisterResult
    {
        Successful, InternalError
    }
}
