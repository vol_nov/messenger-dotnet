﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Messenger.Models
{
    [Serializable]
    public class Attachment : Entity
    {
        [NonSerialized]
        private byte[] _bytes;
        public byte[] Bytes
        {
            get { return _bytes; }
            set { _bytes = value; }
        }
        public string Path { get; set; }
        public Attachment()
        {

        }
        public byte[] GetBytes()
        {
            if (Path != null)
            {
                if (!AttachmentEmoticon.IsEmoticon(Path))
                {
                    using (var fs = File.Open(Path, FileMode.Open))
                    {
                        var result = new byte[fs.Length];
                        fs.ReadAsync(result, 0, result.Length).Wait();
                        return result;
                    }
                }
            }

            return null;
        }

        public static string GetFileName(string path)
        {
            if (path.Contains('\\'))
            {
                return path.Substring(path.LastIndexOf('\\') + 1);
            }
            else
            {
                return path;
            }
        }
    }
}
