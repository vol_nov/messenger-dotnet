﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Messenger.Models
{
    [Serializable]
    public abstract class Entity
    {
        public int Id { get; set; }
        public string TypeName { get { return GetType().Name; } }
        public Entity()
        {
            
        }
        public Entity(int id)
        {
            Id = id;
        }
        
        public new string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }

        public byte[] ToByteArray()
        {
            return Encoding.UTF8.GetBytes(ToString());
        }
    }
}
