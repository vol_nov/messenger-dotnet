﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Messenger.Models
{

    [Serializable]
    public class Message: Entity
    {
        public virtual object Content { get; set; }
        public string SenderUsername { get; set; }
        public string ReceiverUsername { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
