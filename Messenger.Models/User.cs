﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Messenger.Models
{
    [Serializable]
    public class User: Entity
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public ObservableCollection<string> Contacts { get; set; }
        public bool Online { get; set; }
        public User()
        {

        }  
    }
}