﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;

namespace Messenger.Models
{
    [Serializable]
    public class AttachmentImage: Attachment
    {
        public AttachmentImage()
        {
            
        }
        
        public static bool IsImage(string path)
        {
            var regex = new Regex("^*.(PNG|png|JPG|jpg|BMP|bmp)");
            return regex.IsMatch(path);
        }
    }
}
