﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Messenger.Models
{
    [Serializable]
    public class AttachmentEmoticon: Attachment
    {
        public static bool IsEmoticon(string path)
        {
            return AttachmentImage.IsImage(path) && GetFileName(path).StartsWith("emoticon_");
        }
    }
}
