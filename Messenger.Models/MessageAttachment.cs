﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Messenger.Models
{
    [Serializable]
    public class MessageAttachment : Message
    {
        private Attachment _attachment;

        public override object Content
        {
            get
            {
                return _attachment;
            }

            set
            {
                _attachment = value as Attachment;
            }
        }

        public MessageAttachment()
        {

        }
    }
}
