﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Messenger.Models.Extensions
{
    public static class Extensions
    {
        public static IPAddress GetLocalIP()
        {
            IPAddress[] localIPs = Dns.GetHostAddresses(Dns.GetHostName());
            foreach (IPAddress addr in localIPs)
            {
                if (addr.AddressFamily == AddressFamily.InterNetwork)
                {
                    return addr;
                }
            }
            return null;
        }

        public static int Count(this StringBuilder sb, char c)
        {
            int count = 0;
            for (int i = 0; i < sb.Length; i++)
            {
                if (sb[i] == c)
                    count++;
            }

            return count;
        }

        public static byte[] Subarray(this byte[] arr)
        {
            if (Array.LastIndexOf(arr, 0) == arr.Length - 1)
            {
                int index = arr.Length - 1;
                for (int i = arr.Length - 1; i >= 0; i--)
                {
                    if (arr[i] != 0)
                    {
                        index = i;
                        break;
                    }
                }

                byte[] newArr = new byte[index + 1];
                Array.Copy(arr, newArr, index + 1);

                return newArr;
            }
            return arr;
        }
    }
}
