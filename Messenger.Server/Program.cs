﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using Messenger.Models;
using Newtonsoft.Json;
using System.Runtime.InteropServices;
using System.Net.Sockets;
using System.Net;
using Messenger.Server.Services;
using Messenger.Models.Extensions;

namespace Messenger.Server
{
    class Program
    {
        //Шлях до збереження файлу
        const string filePath = "data.bin";

        [DllImport("Kernel32")]
        private static extern bool SetConsoleCtrlHandler(EventHandler handler, bool add);

        private delegate bool EventHandler(CtrlType sig);
        private static EventHandler _handler;
            
        private static ServerMessengerService serverModel;
        private static UdpClient server;
        enum CtrlType
        {
            CTRL_C_EVENT = 0,
            CTRL_BREAK_EVENT = 1,
            CTRL_CLOSE_EVENT = 2,
            CTRL_LOGOFF_EVENT = 5,
            CTRL_SHUTDOWN_EVENT = 6
        }

        //Зберегти файл при виході з програми
        private static bool Handler(CtrlType sig)
        {
            serverModel.Serialize(filePath);
            server.Close();

            Environment.Exit(-1);
            return true;
        }

        //Отримати IP
        
        static void Main(string[] args)
        {
            _handler += new EventHandler(Handler);
            SetConsoleCtrlHandler(_handler, true);
            //Ініціалізуємо сутності на сервері
            
            try
            {
                serverModel = File.Exists(filePath) ?
                    ServerMessengerService.Deserialize(filePath) : new ServerMessengerService();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                serverModel = new ServerMessengerService();
            }

            IPAddress localIP = Extensions.GetLocalIP();
            
            Console.WriteLine("Local IP: " + localIP);

            server = new UdpClient(new IPEndPoint(localIP, 33333));
            
            Console.WriteLine("Server started!!!");

            while (true)
            {
                try
                {
                    //Слухаємо клієнт

                    IPEndPoint remoteIpEndPoint = new IPEndPoint(IPAddress.Any, 33333);

                    byte[] buffer = server.Receive(ref remoteIpEndPoint);

                    string request = Encoding.UTF8.GetString(buffer.ToArray());
                    string dataJSON = request.Substring(request.IndexOf("Data: ") + "Data: ".Length);

                    //В залежності від запиту
                    //Обробляємо дані і викликаємо відповідний метод в ServerMessengerService

                    IPEndPoint currentClient = new IPEndPoint(remoteIpEndPoint.Address, 44444);

                    if (request.StartsWith("SignIn"))
                    {
                        var user = JsonConvert.DeserializeObject(dataJSON, typeof(User)) as User;
                        var signInResult = serverModel.SignIn(user.Username, user.Password);
                        var result = Encoding.UTF8.GetBytes(signInResult.ToString());
                        server.Send(result, result.Length, currentClient);
                    }
                    else if (request.StartsWith("Register"))
                    {
                        var user = JsonConvert.DeserializeObject(dataJSON, typeof(User)) as User;
                        var registerResult = serverModel.Register(user.Username, user.Password);
                        var result = Encoding.UTF8.GetBytes(registerResult.ToString());
                        server.Send(result, result.Length, currentClient);
                    }
                    else if (request.StartsWith("IsUniqueUsername"))
                    {
                        var result = Encoding.UTF8.GetBytes(serverModel.IsUniqueUsername(dataJSON.Replace("\0", "").Trim()).ToString());
                        server.Send(result, result.Length);
                    }
                    else if (request.StartsWith("GetContacts"))
                    {
                        var contacts = serverModel.GetContacts(dataJSON.Replace("\0", "").Trim());
                        var result = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(contacts));
                        server.Send(result, result.Length, currentClient);
                    }
                    else if (request.StartsWith("FindContacts"))
                    {
                        var contacts = serverModel.FindContacts(dataJSON.Replace("\0", "").Trim());
                        var result = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(contacts));
                        server.Send(result, result.Length, currentClient);
                    }
                    else if (request.StartsWith("AddContact"))
                    {
                        var anonymousType = new { Username = "", Contact = "" };
                        dynamic parameters = JsonConvert.DeserializeObject(dataJSON, anonymousType.GetType());
                        serverModel.AddContact(parameters.Username, parameters.Contact);
                    }
                    else if (request.StartsWith("RemoveContact"))
                    {
                        var anonymousType = new { Sender = "", Receiver = "" };
                        dynamic parameters = JsonConvert.DeserializeObject(dataJSON, anonymousType.GetType());
                        serverModel.RemoveContact(parameters.Sender, parameters.Receiver);
                    }
                    else if (request.StartsWith("GetNewMessages"))
                    {
                        var anonymousType = new { Sender = "", Receiver = "", LastMessageId = 0 };
                        dynamic parameters = JsonConvert.DeserializeObject(dataJSON, anonymousType.GetType());
                        var messages = serverModel.GetNewMessages(parameters.Sender, parameters.Receiver, parameters.LastMessageId);
                        var bytesArr = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(messages));
                        server.Send(bytesArr, bytesArr.Length, currentClient);
                    }
                    else if (request.StartsWith("GetAttachment"))
                    {
                        Attachment attachment = JsonConvert.DeserializeObject(dataJSON,
                            typeof(Attachment)) as Attachment;

                        byte[] bytes = attachment.GetBytes();

                        server.Send(bytes, bytes.Length, currentClient);
                    }
                    else if (request.StartsWith("SendMessage"))
                    {
                        string beginningOfRequest = Encoding.UTF8.GetString(buffer.ToArray());

                        if (server.Available > 0)
                        {
                            var received = new StringBuilder(beginningOfRequest);

                            while (server.Available > 0)
                            {
                                Array.Clear(buffer, 0, buffer.Length);
                                buffer = server.Receive(ref remoteIpEndPoint);
                                received.Append(Encoding.UTF8.GetString(buffer));
                            }

                            string receivedStr = received.ToString();
                            dataJSON = receivedStr.Substring(receivedStr.IndexOf(':') + 1).Replace("\0", "").Trim();

                            Console.WriteLine(received.Length);
                        }

                        var message = JsonConvert.DeserializeObject<Message>(dataJSON);
                        int lastMsgId = serverModel.SendMessage(message);
                        var result = (Encoding.UTF8.GetBytes(lastMsgId.ToString()));
                        server.Send(result, result.Length, currentClient);
                    }
                    else
                    {

                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.StackTrace);
                }
            }
        }
    }
}
