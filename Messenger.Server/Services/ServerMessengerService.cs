﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using Messenger.Models;
using Messenger.Models.Enums;
using System.Security.Cryptography;
using System.Collections.ObjectModel;

namespace Messenger.Server.Services
{
    [Serializable]
    public class ServerMessengerService : IMessengerService
    {
        //Прикріплення
        public List<Attachment> Attachments { get; set; }
        //Користувачі
        public List<User> Users { get; set; }
        //Повідомлення
        public List<Message> Messages { get; set; }
        public ServerMessengerService()
        {
            Attachments = new List<Attachment>();
            Users = new List<User>();
            Messages = new List<Message>();
        }

        #region Serialization

        //Збереження в файл
        public void Serialize(string path)
        {
            if (File.Exists(path))
                File.Delete(path);

            using (var fs = File.Open(path, FileMode.Create))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(fs, this);
            }
        }

        //Зчитування з файлу
        public static ServerMessengerService Deserialize(string path)
        {
            ServerMessengerService data;

            using (var fs = File.Open(path, FileMode.Open))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                data = formatter.Deserialize(fs) as ServerMessengerService;
            }

            return data;
        }

        #endregion

        public SignInResult SignIn(string username, string password)
        {
            try
            {
                if (Users.Any(u => u.Username == username && u.Password == password))
                {
                    Users.First(u => u.Username == username).Online = true;
                    return SignInResult.Successful;
                }
                else if (Users.Any(u => u.Username == username && u.Password != password))
                {
                    return SignInResult.PasswordNotCorrect;
                }
                else
                {
                    return SignInResult.NoUserWithSuchUsername;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                return SignInResult.InternalError;
            }
        }

        public bool IsUniqueUsername(string username)
        {
            return !Users.Any(u => u.Username == username);
        }

        public RegisterResult Register(string username, string password)
        {
            RegisterResult res = RegisterResult.Successful;
            try
            {
                int userId = Users.LastOrDefault() == null ? 1 : Users.Last().Id + 1;
                Users.Add(new User() { Id = userId, Username = username, Password = password });
            }
            catch (Exception e)
            {
                res = RegisterResult.InternalError;
                Console.WriteLine(e.StackTrace);
            }
            return res;
        }

        public string[] GetContacts(string username)
        {
            return Users.FirstOrDefault(u => u.Username == username)?.Contacts?.ToArray();
        }

        public Message[] GetNewMessages(string sender, string receiver, int lastMsgId)
        {
            var messages = Messages.Where(m => ((m.SenderUsername == sender && m.ReceiverUsername == receiver)
                 || (m.SenderUsername == receiver && m.ReceiverUsername == sender)));
            
            return messages.Where(m => m.Id > lastMsgId).ToArray();
        }

        public int SendMessage(Message message)
        {
            int messageId = Messages.LastOrDefault() == null ? 1 : Messages.Last().Id + 1;

            message.Id = messageId;

            dynamic messageContent = message.Content;

            if (!(messageContent is string))
            {
                string path = messageContent.Path;

                if (!AttachmentEmoticon.IsEmoticon(path))
                {
                    int attachmentId = Attachments.LastOrDefault() == null ? 1 : Attachments.Last().Id + 1;

                    Attachment attachment = new Attachment() {
                        Id = attachmentId,
                        Path = Path.GetFullPath("../../Attachments/" + GetEncodedHash(path) + path.Substring(path.LastIndexOf('.')))
                    };

                    Attachments.Add(attachment);

                    byte[] byteArr = messageContent.Bytes;

                    Console.WriteLine(attachment.Path);
                    Console.WriteLine(byteArr.Length);

                    File.WriteAllBytes(attachment.Path, byteArr);

                    messageContent.Bytes = null;

                    message.Content = attachment;
                }
            }


            Messages.Add(message);

            return messageId;
        }

        private static string GetEncodedHash(string path)
        {
            string salt = "adhasdhasdhas" + DateTime.Now.Ticks.ToString();

            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] digest = md5.ComputeHash(Encoding.UTF8.GetBytes(path + salt));
            string base64digest = Convert.ToBase64String(digest, 0, digest.Length);

            return base64digest.Substring(0, base64digest.Length - 2).Replace('/', ' ').Replace('\\', ' ');
        }
        
        public string[] FindContacts(string username)
        {
            return Users.Where(u => u.Username.ToLower()
                        .Contains(username.ToLower()))
                        .Select(u => u.Username).ToArray();
        }

        public void AddContact(string username, string contact)
        {
            var user = Users.FirstOrDefault(u => u.Username == username);
            var userToContact = Users.FirstOrDefault(u => u.Username == contact);

            if (user.Contacts == null)
                user.Contacts = new ObservableCollection<string>();

            if (userToContact.Contacts == null)
                userToContact.Contacts = new ObservableCollection<string>();

            if (user.Contacts.All(u => u != contact))
            {
                user.Contacts.Add(contact);
                userToContact.Contacts.Add(username);
            }
        }

        public void RemoveContact(string sender, string receiver)
        {
            var userSender = Users.FirstOrDefault(u => u.Username == sender);
            var userReceiver = Users.FirstOrDefault(u => u.Username == receiver);
            if (userSender != null)
            {
                if (userSender.Contacts != null)
                {
                    userSender.Contacts.Remove(receiver);
                    if (userReceiver != null)
                    {
                        if (userReceiver.Contacts != null)
                        {
                            userReceiver.Contacts.Remove(sender);
                        }
                    }
                }
            }
        }

        public void GoOffline(string username)
        {
            Users.FirstOrDefault(u => u.Username == username).Online = false;
        }
    }
}
