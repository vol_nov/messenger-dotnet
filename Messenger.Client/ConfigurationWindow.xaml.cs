﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Messenger.Client
{
    /// <summary>
    /// Interaction logic for ConfigurationWindow.xaml
    /// </summary>
    public partial class ConfigurationWindow : Window
    {
        public ConfigurationWindow()
        {
            InitializeComponent();

            textBoxServerIPAddress.Text = Settings.GetInstance().ServerIPAddress.ToString();
            textBoxTimeout.Text = ((int)Settings.GetInstance().NewMessagesTimeout.TotalMilliseconds).ToString();
        }

        private void buttonSave_Click(object sender, RoutedEventArgs e)
        {
            IPAddress address;

            if (!IPAddress.TryParse(textBoxServerIPAddress.Text, out address))
            {
                MessageBox.Show("Enter server ip address properly!");
                return;
            }
            else
            {
                Settings.GetInstance().ServerIPAddress = new IPEndPoint(address, 33333);
            }

            int timeout;

            if (!int.TryParse(textBoxTimeout.Text, out timeout))
            {
                MessageBox.Show("Enter timeout properly!");
                return;
            }
            else
            {
                Settings.GetInstance().NewMessagesTimeout = TimeSpan.FromMilliseconds(timeout);
            }

            DialogResult = true;
            Close();
        }
    }
}
