﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Messenger.Client
{
    /// <summary>
    /// Interaction logic for EmoticonsWindow.xaml
    /// </summary>
    public partial class EmoticonsWindow : Window
    {
        public string SelectedEmoticonPath { get; set; }

        public EmoticonsWindow()
        {
            InitializeComponent();

            //Відкриваємо папку з іконками
            var emoticons =  new DirectoryInfo(@"..\..\Emoticons").GetFiles();

            double x = 0, y = 0;
            double countOfElements = 15;
            //Виводимо кожну іконку
            canvas.CacheMode = new BitmapCache();
            foreach (var emoticon in emoticons)
            {
                Dispatcher.Invoke(new Action(() => { 
                    Image img = new Image() { Source = new BitmapImage(new Uri(emoticon.FullName)) };

                    img.Width = 26;
                    img.Height = 26;

                    img.MouseDown += (s, e) => {
                        SelectedEmoticonPath = ((s as Image).Source as BitmapImage).UriSource.LocalPath;
                        DialogResult = true;
                        Close();
                    };

                    Canvas.SetLeft(img, x);
                    Canvas.SetTop(img, y);

                    canvas.Children.Add(img);

                    x += img.Width;
                    if (x == img.Width * countOfElements)
                    {
                        x = 0;
                        y += img.Height;
                    }
                }), DispatcherPriority.Render);
            }
        }
        
    }
}
