﻿using Messenger.Client.Services;
using Messenger.Models;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Collections.ObjectModel;
namespace Messenger.Client
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// Головне вікно програми
    public partial class MainWindow : Window
    {
        private ClientMessengerService clientModel;
        private User currentUser;
        private DispatcherTimer timer;
        private int lastMessageId;
        private readonly string emoticonsPath = @"..\..\Emoticons\";
        private readonly double emoticonsWidth = 48;
        private readonly double emoticonsHeight = 48;
        private bool attachmentIsSending = false;

        public MainWindow(string username)
        {
            InitializeComponent();

            lastMessageId = -1;
            clientModel = new ClientMessengerService();
            
            //Отримуємо контакти з сервера
            var contacts = clientModel.GetContacts(username);

            if (contacts == null)
                contacts = new string[0];

            currentUser = new User() { Username = username, Contacts = new ObservableCollection<string>(contacts) };

            //Відображуємо контакти в ListBox
            listBoxContacts.ItemsSource = new ObservableCollection<string>(currentUser.Contacts);
            listBoxContacts.Items.Refresh();

            //Виділяємо перший контакт
            if (currentUser.Contacts.Count > 0)
            {
                listBoxContacts.SelectedIndex = 0;
            }

            Title = "Messenger: " + currentUser.Username;

            //Встановлюємо таймер на перевірку нових повідомлень
            timer = new DispatcherTimer();
            timer.Interval = Settings.GetInstance().NewMessagesTimeout;
            timer.Tick += Timer_Tick;
            timer.Start();
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            //Якщо не надсилається прикріплення
            //(чому? тому що прикріплення набагато важчі за звичайні текстові повідомлення)

            if (!attachmentIsSending)
            {
                //Отримуємо нові повідомлення
                //Відображаємо їх в RichTextBox

                Message[] messages = clientModel.GetNewMessages(currentUser.Username,
                    listBoxContacts.SelectedItem as string, lastMessageId);
                if (messages.Length > 0)
                {
                    foreach (var message in messages)
                    {
                        AddRichTextBoxMessage(message);
                    }

                    //Записуємо id останнього повідомлення
                    lastMessageId = messages.Last().Id;
                }
            }
        }

        private void buttonEmoticon_Click(object sender, RoutedEventArgs e)
        {
            //Показати вікно смайлів
            EmoticonsWindow emoticons = new EmoticonsWindow();
            if (emoticons.ShowDialog() == true)
            {
                //Якщо все ок
                //Зчитати смайл і надіслати на сервер повідомлення
                
                var emoticonPath = emoticons.SelectedEmoticonPath;
                var emoticonMessage = new MessageAttachment()
                {
                    CreationDate = DateTime.Now,
                    SenderUsername = currentUser.Username,
                    ReceiverUsername = listBoxContacts.SelectedItem as string,
                    Content = new AttachmentEmoticon() { Path = emoticonPath }
                };
                lastMessageId = clientModel.SendMessage(emoticonMessage);
                AddRichTextBoxMessage(emoticonMessage);

                richTextBox.ScrollToEnd();
            }
        }

        private void buttonAdd_Click(object sender, RoutedEventArgs e)
        {
            //Додати новий контакт
            var userToAdd = listBoxContacts.SelectedItem as string;
            //Якщо користувач не має таких контактів або цей контакт не є ним
            if (!currentUser.Contacts.Any(u => u == userToAdd) && currentUser.Username != userToAdd)
            {
                clientModel.AddContact(currentUser.Username, userToAdd);
                currentUser.Contacts.Add(userToAdd);
            }
        }

        private void buttonFind_Click(object sender, RoutedEventArgs e)
        {
            //Пошук користувачів в контактах

            if (textBoxContacts.Text.Length == 0)
                listBoxContacts.ItemsSource = currentUser.Contacts;
            else
                listBoxContacts.ItemsSource = clientModel.FindContacts(textBoxContacts.Text);

            listBoxContacts.Items.Refresh();
        }

        private void buttonSend_Click(object sender, RoutedEventArgs e)
        {
            //Надсилання текстового повідомлення
            if (currentUser.Contacts.Count > 0)
            {
                var messageText = new MessageText()
                {
                    CreationDate = DateTime.Now,
                    SenderUsername = currentUser.Username,
                    ReceiverUsername = listBoxContacts.SelectedItem as string,
                    Content = textBoxMessage.Text
                };
                lastMessageId = clientModel.SendMessage(messageText);

                AddRichTextBoxMessage(messageText);

                textBoxMessage.Text = "";
                
                richTextBox.ScrollToEnd();
            }
        }

        private void listBoxContacts_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //Очищення повідомлень при зміні контакту
            lastMessageId = -1;
            richTextBox.Document.Blocks.Clear(); 
        }

        private void AddRichTextBoxMessage(Message message)
        {
            //Додати повідомлення
            if (message.Content is string)
            {
                //Якщо рядок додаємо як текст
                richTextBox.Document.Blocks.Add(new Paragraph(new Run(message.SenderUsername + ": " + message.Content as string)));
            }
            else
            {
                //Інакше перевіряємо
                dynamic content = message.Content;
                string path = content.Path;
                
                //Якщо смайл
                if (AttachmentEmoticon.IsEmoticon(path))
                {
                    string fileName = Attachment.GetFileName(path);
                    var emoticonUri = new Uri(System.IO.Path.GetFullPath(emoticonsPath + fileName));

                    var emoticonImage = new Image()
                    {
                        Source = new BitmapImage(emoticonUri),
                        Width = emoticonsWidth,
                        Height = emoticonsHeight,
                        HorizontalAlignment = HorizontalAlignment.Left
                    };

                    //Додаємо локальний смайл
                    richTextBox.Document.Blocks.Add(new Paragraph(new Run(message.SenderUsername + ": ")));
                    richTextBox.Document.Blocks.Add(new BlockUIContainer(emoticonImage));
                }
                else 
                {
                    string contentPath = content.Path;
                    content.Path = System.IO.Path.GetFullPath(@"..\..\Files\" + Attachment.GetFileName(contentPath));
                    contentPath = content.Path;

                    if (AttachmentImage.IsImage(contentPath))
                    {
                        try
                        {
                            var emoticonImage = new Image() {
                                Source = new BitmapImage(new Uri(contentPath)),
                                HorizontalAlignment = HorizontalAlignment.Left
                            };

                            richTextBox.Document.Blocks.Add(new Paragraph(new Run(message.SenderUsername + ": ")));
                            richTextBox.Document.Blocks.Add(new BlockUIContainer(emoticonImage));
                        }
                        catch
                        {

                        }
                    }
                    else
                    {
                        Hyperlink link = new Hyperlink();

                        link.IsEnabled = true;
                        link.Inlines.Add(Attachment.GetFileName(contentPath));
                        link.NavigateUri = new Uri(contentPath);

                        link.RequestNavigate += (sender, args) => {
                            Process.Start(args.Uri.AbsolutePath.ToString());
                            args.Handled = true;
                        };
                        
                        richTextBox.Document.Blocks.Add(new Paragraph(new Run(message.SenderUsername + ": ")));
                        richTextBox.Document.Blocks.Add(new Paragraph(link));
                    }
                }
            }
        }

        
        private void buttonFile_Click(object sender, RoutedEventArgs e)
        {
            //Відкриваємо діалог для вибору файлу
            OpenFileDialog dialog = new OpenFileDialog();
            if (dialog.ShowDialog() == true)
            {
                Attachment attachment = new Attachment() { Path = dialog.FileName };

                string newPath = @"..\..\Files\" + Attachment.GetFileName(attachment.Path);
                
                if (File.Exists(newPath))
                    File.Delete(newPath);

                File.Copy(attachment.Path, newPath);


                //Зчитуємо вміст файлу
                attachment.Bytes = attachment.GetBytes();

                MessageAttachment messageAttachment = new MessageAttachment() {
                    Content = attachment,
                    CreationDate = DateTime.Now,
                    SenderUsername = currentUser.Username,
                    ReceiverUsername = listBoxContacts.SelectedItem as string
                };

                attachmentIsSending = true;

                //Надсилаємо повідомлення з прикріпленням
                lastMessageId = clientModel.SendMessage(messageAttachment);
                AddRichTextBoxMessage(messageAttachment);

                attachmentIsSending = false;

                attachment.Bytes = null;

                richTextBox.ScrollToEnd();
            }
        }

        private void buttonRemove_Click(object sender, RoutedEventArgs e)
        {
            //Видалення контакту
            if (listBoxContacts.SelectedItem != null)
            {
                string receiver = listBoxContacts.SelectedItem as string;
                clientModel.RemoveContact(currentUser.Username, receiver);
                currentUser.Contacts.Remove(receiver);

                listBoxContacts.ItemsSource = currentUser.Contacts;
                listBoxContacts.Items.Refresh();
            }
        }

        private void buttonSettings_Click(object sender, RoutedEventArgs e)
        {
            //Вікно налаштувань
            ConfigurationWindow config = new ConfigurationWindow();
            config.ShowDialog();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            clientModel.Dispose();
        }
    }
}
