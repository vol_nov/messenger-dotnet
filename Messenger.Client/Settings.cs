﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Messenger.Client
{
    //Клас настройок
    //Зберігаються ip адреса сервера та періодичність перевірки повідомлень
    public class Settings
    {
        private const string _settingsFilePath = "settings.bin";
        private IPEndPoint _serverIPAddress;
        private TimeSpan _newMessagesTimeout;
        public IPEndPoint ServerIPAddress
        {
            get
            {
                return _serverIPAddress;
            }
            set
            {
                _serverIPAddress = value;
                SaveChanges();
            }
        }

        public TimeSpan NewMessagesTimeout
        {
            get
            {
                return _newMessagesTimeout;
            }
            set
            {
                _newMessagesTimeout = value;
                SaveChanges();
            }
        }

        private static Settings _settings;

        private Settings()
        {
            //Перевіряємо чи існує файл настройок
            if (!File.Exists(_settingsFilePath))
            {
                SetDefaultSettings();
            }
            else
            {
                //Якщо існує, зчитуємо настройки
                try
                {
                    //Формат запису в файл
                    //ip адреса, періодичність перевірки нових повідомлень
                    string settings = File.ReadAllText(_settingsFilePath);
                    int index = settings.IndexOf(',');
                    _serverIPAddress = new IPEndPoint(IPAddress.Parse(settings.Substring(0, index)), 33333);
                    _newMessagesTimeout = TimeSpan.FromMilliseconds(int.Parse(settings.Substring(index + 1)));
                } catch(Exception e) {
                    //Інакше виводимо повідомлення про помилку і встановлюємо настройки по дефолту
                    MessageBox.Show(e.StackTrace);
                    SetDefaultSettings();
                }
            }
        }
       
        public static Settings GetInstance()
        {
            if (_settings == null)
                _settings = new Settings();

            return _settings;
        }

        private void SaveChanges()
        {
            using (var fs = File.Open(_settingsFilePath, FileMode.Create))
            {
                string settings = $"{_serverIPAddress}, {_newMessagesTimeout.TotalMilliseconds}";
                byte[] settingsBytes = Encoding.UTF8.GetBytes(settings);
                fs.Write(settingsBytes, 0, settingsBytes.Length);
            }   
        }

        private void SetDefaultSettings()
        {
            _serverIPAddress = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 33333);
            _newMessagesTimeout = TimeSpan.FromMilliseconds(500);
        }
    }
}
