﻿using Messenger.Client.Services;
using Messenger.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Messenger.Client
{
    /// <summary>
    /// Interaction logic for SignIn.xaml
    /// </summary>
    /// Вікно входу
    public partial class SignIn : Window
    {
        //Оголошення клієнтського сервісу
        ClientMessengerService _clientModel;

        public SignIn()
        {
            InitializeComponent();
            _clientModel = new ClientMessengerService();
        }

        private void buttonSignIn_Click(object sender, RoutedEventArgs e)
        {
            //Вхід в систему
            SignInResult res = _clientModel.SignIn(textBoxUsername.Text, textBoxPassword.Password);

            //Надсилається username i password, якщо все добре відображаємо головне вікно
            if (res == SignInResult.Successful)
            {
                _clientModel.Dispose();
                MainWindow mainWindow = new MainWindow(textBoxUsername.Text);
                mainWindow.Show();
                Close();
            }
            else
            {
                //Інакше виводимо повідомлення про помилку
                MessageBox.Show(res.ToString());
            }
        }

        private void buttonRegister_Click(object sender, RoutedEventArgs e)
        {
            if (_clientModel.IsUniqueUsername(textBoxUsername.Text))
            {
                MessageBox.Show(_clientModel.Register(textBoxUsername.Text, textBoxPassword.Password).ToString());
            }
            else
            {
                MessageBox.Show("Username is not unique!");
            }
        }

        private void buttonSettings_Click(object sender, RoutedEventArgs e)
        {
            ConfigurationWindow config = new ConfigurationWindow();
            config.ShowDialog();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            //_clientModel.Dispose();
        }
    }
}
