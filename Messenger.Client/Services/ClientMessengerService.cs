﻿using Messenger.Models;
using Messenger.Models.Enums;
using Messenger.Models.Extensions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace Messenger.Client.Services
{
    public class ClientMessengerService: IMessengerService, IDisposable
    {
        private byte[] buffer = new byte[1024];
        private IPEndPoint server;
        private IPEndPoint client;
        private UdpClient senderClient;
        private UdpClient receiverClient;
        public ClientMessengerService()
        {
            server = Settings.GetInstance().ServerIPAddress;
            client = new IPEndPoint(Settings.GetInstance().ServerIPAddress.Address, 44444);
            senderClient = CreateSenderClient();
            receiverClient = CreateReceiverClient();
        }

        //Створення сокету
        private UdpClient CreateSenderClient()
        {
            UdpClient client = new UdpClient();
            client.Connect(Settings.GetInstance().ServerIPAddress);
            return client;
        }

        private UdpClient CreateReceiverClient()
        {
            return new UdpClient(new IPEndPoint(Extensions.GetLocalIP(), 44444)); 
        }
        
        //Розпарсити значення перерахування(enum) з масиву байтів
        private T ParseEnumFromBytes<T>(byte[] enumBytes) where T : struct
        {
            T res;
            string resultStr = Encoding.UTF8.GetString(enumBytes).Replace("\0", "");
            Enum.TryParse(resultStr, out res);
            return res;
        }

        //Вхід в програму
        public SignInResult SignIn(string username, string password)
        {
            //Створюємо запит
            string request = "SignIn, Data: " + (new User() { Username = username, Password = password }).ToString();
            byte[] requestBytes = Encoding.UTF8.GetBytes(request);

            senderClient.Send(requestBytes, requestBytes.Length); //Надсилаємо масив байтів на сервер
            
            byte[] result = receiverClient.Receive(ref client);
            
            //Отримуємо відповідь
            SignInResult res = ParseEnumFromBytes<SignInResult>(result);
            
            return res; //Повертаємо результат
        }

        //Перевіряємо чи ім'я користувача унікальне
        public bool IsUniqueUsername(string username)
        {
            string requestUniqueUsername = "IsUniqueUsername, Data: " + username;
            byte[] requestBytes = Encoding.UTF8.GetBytes(requestUniqueUsername);
            
            senderClient.Send(requestBytes, requestBytes.Length);
            
            byte[] result = receiverClient.Receive(ref client);

            var resultStr = Encoding.UTF8.GetString(result).Trim().Replace("\0", "");
            
            return bool.Parse(resultStr);
        }

        //Реєстрація користувача
        public RegisterResult Register(string username, string password)
        {
            string request = "Register, Data: " + (new User() { Username = username, Password = password }).ToString();
            byte[] requestBytes = Encoding.UTF8.GetBytes(request);

            senderClient.Send(requestBytes, requestBytes.Length);

            byte[] result = receiverClient.Receive(ref client);

            RegisterResult res = ParseEnumFromBytes<RegisterResult>(result);
            
            return res;
        }
        
        //Отримання контактів користувача
        public string[] GetContacts(string username)
        {
            string requestGetContacts = "GetContacts, Data: " + username;
            byte[] requestBytes = Encoding.UTF8.GetBytes(requestGetContacts);

            senderClient.Send(requestBytes, requestBytes.Length);

            byte[] result = receiverClient.Receive(ref client);

            if (result == null)
                return null;

            var resultStr = Encoding.UTF8.GetString(result).Trim().Replace("\0", "");
            
            return JsonConvert.DeserializeObject<string[]>(resultStr);
        }

        //Підгрузка прикріплень з сервера
        private void ResolveAttachmentPath(Message[] messages)
        {
            if (messages != null)
            {
                foreach (var message in messages)
                {
                    dynamic messageContent = message.Content;

                    if (!(messageContent is string))
                    {
                        string path = messageContent.Path;
                        if (AttachmentEmoticon.IsEmoticon(path))
                        {
                            messageContent.Path = Path.GetFullPath(@"..\..\Emoticons\" + Attachment.GetFileName(path));
                        }
                        else
                        {
                            string newFileName = @"..\..\Files\" + Attachment.GetFileName(path);
                            if (!File.Exists(newFileName))
                            {
                                byte[] bytesArr = GetAttachment(new Attachment() { Path = path });
                                using (var fs = File.Create(newFileName))
                                {
                                    fs.Write(bytesArr, 0, bytesArr.Length);
                                }
                            }
                            messageContent.Path = newFileName;
                        }
                    }
                }
            }
        }

        //Отримання нових повідомлень
        public Message[] GetNewMessages(string sender, string receiver, int lastMsgId)
        {
            string requestGetNewMessages = "GetNewMessages, Data: " +
                JsonConvert.SerializeObject(new
                {
                    Sender = sender,
                    Receiver = receiver,
                    LastMessageId = lastMsgId
                });

            byte[] requestBytes = Encoding.UTF8.GetBytes(requestGetNewMessages);
            
            senderClient.Send(requestBytes, requestBytes.Length);

            buffer = receiverClient.Receive(ref client);

            string dataJSON = Encoding.UTF8.GetString(buffer);

            if (receiverClient.Available > 0)
            {
                var received = new StringBuilder(dataJSON);

                while (receiverClient.Available > 0)
                {
                    Array.Clear(buffer, 0, buffer.Length);
                    buffer = receiverClient.Receive(ref client);
                    received.Append(Encoding.UTF8.GetString(buffer));
                }

                string receivedStr = received.ToString();
                dataJSON = receivedStr.Replace("\0", "").Trim();
            }

            var messages = JsonConvert.DeserializeObject<Message[]>(dataJSON);
            
            ResolveAttachmentPath(messages);
            
            Array.Clear(buffer, 0, buffer.Length);

            return messages;
        }
        //Надсилання повідомлення
        public int SendMessage(Message message)
        {
            string messageJSON = JsonConvert.SerializeObject(message);
            string requestGetNewMessages = $"SendMessage, Data: " + messageJSON;
            
            var requestBytes = Encoding.UTF8.GetBytes(requestGetNewMessages);

            int bufferSize = 32000;
            for (int i = 0; i < requestBytes.Length; i += bufferSize)
            {
                int length = requestBytes.Length - i < bufferSize ? requestBytes.Length - i : bufferSize;
                senderClient.Send(requestBytes.Skip(i).Take(length).ToArray(), length);
            }

            byte[] resultLength = receiverClient.Receive(ref client);

            int lastId;
            int.TryParse(Encoding.UTF8.GetString(resultLength), out lastId);
            
            return lastId;
        }
        
        //Пошук по контактам
        public string[] FindContacts(string username)
        {
            string requestFindContacts = "FindContacts, Data: " + username;
            byte[] requestBytes = Encoding.UTF8.GetBytes(requestFindContacts);
            
            senderClient.Send(requestBytes, requestBytes.Length);

            byte[] result = receiverClient.Receive(ref client);

            var resultStr = Encoding.UTF8.GetString(result).Trim().Replace("\0", "");
            
            return JsonConvert.DeserializeObject<string[]>(resultStr);
        }

        //Додаємо контакт
        public void AddContact(string username, string contact)
        {
            string requestAddContact = "AddContact, Data: " + JsonConvert.SerializeObject(new { Username = username, Contact = contact });
            byte[] requestBytes = Encoding.UTF8.GetBytes(requestAddContact);

            senderClient.Send(requestBytes, requestBytes.Length);
        }

        //Загрузка прикріплень з сервера
        public byte[] GetAttachment(Attachment attachment)
        {
            string requestGetAttachment = "GetAttachment, Data: " + JsonConvert.SerializeObject(attachment);
            byte[] requestBytes = Encoding.UTF8.GetBytes(requestGetAttachment);
        
            senderClient.Send(requestBytes, requestBytes.Length);
            buffer = receiverClient.Receive(ref client);

            List<byte> sb = new List<byte>(buffer.Subarray());
            
            while (receiverClient.Available > 0)
            {
                Array.Clear(buffer, 0, buffer.Length);
                buffer = receiverClient.Receive(ref client);
                sb.AddRange(buffer.Subarray());
            }
            
            Array.Clear(buffer, 0, buffer.Length);

            return sb.ToArray();
        }

        //Видалення контакту
        public void RemoveContact(string sender, string receiver)
        {
            string requestRemoveContact = "RemoveContact, Data: " + JsonConvert.SerializeObject(
                new
                {
                    Sender = sender,
                    Receiver = receiver
                });

            byte[] requestBytes = Encoding.UTF8.GetBytes(requestRemoveContact);
            senderClient.Send(requestBytes, requestBytes.Length);
        }

        public void GoOffline(string username)
        {
            string requestGoOffline = $"GoOffline, Data: {username}";
            byte[] requestBytes = Encoding.UTF8.GetBytes(username);

            senderClient.Send(requestBytes, requestBytes.Length);
        }

        public void Dispose()
        {
            senderClient.Close();
            receiverClient.Close();
        }
    }
}
